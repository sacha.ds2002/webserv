/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Utils.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/28 10:42:14 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/14 16:06:34 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include  "../webserv.h"

namespace Utils
{
	// Reads a file, taking the fd as identifier
	std::string ReadFileWithFd(int fd) 
	{
		std::string content;
		char buffer[1000];
		ssize_t bytesRead;

		while ((bytesRead = read(fd, buffer, sizeof(buffer))) > 0)
			content.append(buffer, bytesRead);

		if (bytesRead == -1)
			throw HttpException(500);

		return content;
	}

	// Creates a file, returns true if directory already exists or if created
	bool CreateDirectory(const std::string& path)
	{
		// Try creating the directory
		int status = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (status == 0)
			return true;
		else if (errno == EEXIST)
			return true;
		else
			return false;
	}

	// Creates a file, returns false if file couldn't be created
	bool CreateFile(const std::string& path, const std::string& content)
	{
		std::ofstream file(path.c_str(), std::ios::binary);
		if (file.is_open())
		{
			file.write(content.c_str(), content.size());
			file.close();
			return true;
		} 
		else
			return false;
	}

	// Extract the filename from the body string
	std::string ExtractFilename(const std::string& body)
	{
		// Find the position of "filename="
		std::size_t pos = body.find("filename=");
		if (pos == std::string::npos)
			throw HttpException(500); // Return empty string if "filename=" is not found
		
		// Find the opening double quote after "filename="
		std::size_t start = body.find("\"", pos);
		if (start == std::string::npos)
			return ""; // Return empty string if opening double quote is not found
		
		// Find the closing double quote after the opening double quote
		std::size_t end = body.find("\"", start + 1);
		if (end == std::string::npos)
			return ""; // Return empty string if closing double quote is not found
		
		// Extract the filename substring between the opening and closing double quotes
		return body.substr(start + 1, end - start - 1);
	}

	bool StringIsEmptyLine(const std::string& str)
	{
		for (std::string::const_iterator it = str.begin(); it != str.end(); ++it)
		{
			if (isprint(static_cast<unsigned char>(*it)))
				return false;  // Found a printable character
		}
		return true;  // All characters are unprintable
	}



	// Extract a file's content from a multipart body
	std::string ExtractFileContent(const std::string &contentType, const std::string &body) 
	{
		size_t bIndex = contentType.find("boundary=") + 9;
		std::string boundary = contentType.substr(bIndex);
		std::string delimiter = "--" + boundary;

		// Locate the start of the file content
		size_t start = body.find("\r\n\r\n");
		if (start == std::string::npos) {
			return "";
		}
		start += 4; // Skip past the header's \r\n\r\n

		// Locate the end of the file content
		size_t end = body.find(delimiter, start);
		if (end == std::string::npos) {
			return "";
		}

		// Extract the content, ensuring no boundary markers are included
		std::string content = body.substr(start, end - start);

		// Trim trailing \r\n if present
		if (content.size() >= 2 && content.compare(content.size() - 2, 2, "\r\n") == 0) {
			content.erase(content.size() - 2);
		}

		return content;
	}
}