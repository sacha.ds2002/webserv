#include "../includes/ConfigParser.hpp"
#include <iostream>
#include "../webserv.h"

// ------------------------- Canonical Form -------------------------
// Constructor
ConfigParser::ConfigParser(const std::string& configFilePath) {
	// Initialize variables
	_state = SEARCHING_SERVER;
	_serverKeyword = "server";
	_locationKeyword = "location";
	_lineNum = 0;
	_line = "";
	_debug = false;

	// Open the config file
	std::ifstream file(configFilePath.c_str());
	if (!file.is_open()) {
		throw std::invalid_argument("Could not open the config file.");
	}

	// Parse the config file and store data to the servers classes
	_Parse(file);

	// Close the config file
	file.close();

	// Check if there is at least one server
	if (_servers.empty()) {
		throw std::invalid_argument("No server found in the config file.");
	}

	std::cout << GREEN << "Config file parsed successfully!" << RESET << std::endl;
}

// Copy constructor
ConfigParser::ConfigParser(const ConfigParser& other) {
	_servers = other._servers;
}

// Assignment operator
ConfigParser& ConfigParser::operator=(const ConfigParser& other) {
	if (this != &other) {
		_servers = other._servers;
		_state = other._state;
		_serverKeyword = other._serverKeyword;
		_locationKeyword = other._locationKeyword;
		_lineNum = other._lineNum;
		_line = other._line;
		_debug = other._debug;
	}
	return *this;
}

// Destructor
ConfigParser::~ConfigParser() {}

// ------------------------- Getters -------------------------
std::vector<Server> ConfigParser::GetServers() {
	return _servers;
}

// ------------------------- Setters -------------------------
void ConfigParser::SetServers(const std::vector<Server>& servers) {
	_servers = servers;
}

// ------------------------- Helpers -------------------------
// Remove comments from the _line
void ConfigParser::_RemoveComments(std::string& _line) {
	size_t pos = _line.find("#");
	if (pos != std::string::npos) {
		_line.erase(pos);
	}
}

// Trim the string
void ConfigParser::_Trim(std::string& str) {
	str.erase(0, str.find_first_not_of(" \t\n\r\f\v"));
	str.erase(str.find_last_not_of(" \t\n\r\f\v") + 1);
}

// Parse arguments in a string separated with a space into a vector
// Example: "arg1 arg2 arg3" -> {"arg1", "arg2", "arg3"}
std::vector<std::string> ConfigParser::_GetVectorFromElementsInString(std::string& value) {
	std::vector<std::string> arguments;
	size_t pos = 0;
	while ((pos = value.find(" ")) != std::string::npos) {
		std::string argument = value.substr(0, pos);
		_Trim(argument);
		arguments.push_back(argument);
		value.erase(0, pos + 1);
	}
	_Trim(value);
	arguments.push_back(value);
	return arguments;
}

void ConfigParser::printServers() const {
	for (size_t i = 0; i < _servers.size(); i++) {
		std::cout << "-------------------- Server " << i + 1 << " --------------------" << std::endl;
		_servers[i].PrintInfo();
	}
}

// ------------------------- States -------------------------
// Search for the server keyword
void ConfigParser::_SearchingServer()
{
	// convert the _lineNum into a string for error handling purpose
	std::ostringstream oss;
    oss << _lineNum;
    std::string lineNum = oss.str();

	// Check if the line starts with "server"
	if (_line.substr(0, 6) == _serverKeyword) {
		// Check if there is something after "server"
		if (_line.size() > _serverKeyword.size()) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": No characters allowed after '" + _serverKeyword + "' keyword.");
		}

		// Create a new server object
		Server server;
		_servers.push_back(server);

		// Change the parsing state
		_state = SEARCHING_SEVER_BRACKET;

		if (_debug) {
			std::cout << _lineNum << ": Server found." << std::endl;
		}
	} else {
		throw std::invalid_argument("Parsing Error: at line " + lineNum + ": Expected '" + _serverKeyword + "' keyword.");
	}
}

// Search for the server start bracket
void ConfigParser::_SearchingServerBracket() {
	// convert the _lineNum into a string for error handling purpose
	std::ostringstream oss;
    oss << _lineNum;
    std::string lineNum = oss.str();

	// Check if the line starts with "{"
	if (_line[0] == '{') {
		if (_line.size() > 1) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": No characters allowed after '{'.");
		}

		// Change the parsing state
		_state = SEARCHING_SERVER_DIRECTIVE;

		if (_debug) {
			std::cout << _lineNum << ": Start '{' of a server." << std::endl;
		}
	} else {
		throw std::invalid_argument("Parsing Error: at line " + lineNum + ": Expected '{'.");
	}
}

// Search for the server directive, parse it and store the data
void ConfigParser::_SearchingServerDirective() 
{
	// convert the _lineNum into a string for error handling purpose
	std::ostringstream oss;
    oss << _lineNum;
    std::string lineNum = oss.str();

	// Check if it is the end of the server context
	if (_line[0] == '}') {
		if (_line.size() > 1) {
			throw std::invalid_argument("Parsing Error: at _line " + lineNum + ": No characters allowed after '}'.");
		}
		_state = SEARCHING_SERVER;
		if (_debug) {
			std::cout << _lineNum << ": End '}' of a server." << std::endl;
		}
	} else {
		// Parse the directive

		// Check if there is a ; at the end of the _line
		if (_line[_line.size() - 1] != ';' && _line.substr(0, _locationKeyword.size()) != _locationKeyword) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": Expected ';'.");
		}

		// Remove ';' from the end of the _line
		if (_line[_line.size() - 1] == ';') {
			_line.erase(_line.size() - 1);
		}

		// Parse key
		std::string key = _line.substr(0, _line.find_first_of(" \t"));

		// Parse value
		std::string value = _line.substr(key.size());
		_Trim(value);

		if (_debug) {
			std::cout << _lineNum << ": Key: " << key << ", Value: " << value << std::endl;
		}

		// Check if the key is valid
		try {
			if (key == "port") {
				_servers.back().SetPort(value);
			} else if (key == "host") {
				_servers.back().SetHost(value);
			} else if (key == "server_name") {
				_servers.back().SetServerName(value);
			} else if (key == "root") {
				_servers.back().SetRoot(value);
			} else if (key == "default_file") {
				_servers.back().SetDefaultFile(value);
			} else if (key == "error_page") {
				std::string errorCode = value.substr(0, value.find_first_of(" \t"));
				std::string errorPath = value.substr(errorCode.size());
				_Trim(errorPath);
				_servers.back().UpdateErrorPagePath(errorCode, errorPath);
			} else if (key == "client_max_body_size") {
				_servers.back().SetClientMaxBodySize(std::atoi(value.c_str()));
			} else if (key == "location") {
				_servers.back().AddLocation();
				_state = SEARCHING_LOCATION_BRACKET;
				_servers.back().SetLastLocationRoute(value);
			} else {
				throw std::invalid_argument("Invalid key.");
			}
		} catch (const std::exception& e) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": " + std::string(e.what()));
		}
	}
}

// Search for the location start bracket
void ConfigParser::_SearchingLocationBracket() 
{
	// convert the _lineNum into a string for error handling purpose
	std::ostringstream oss;
    oss << _lineNum;
    std::string lineNum = oss.str();

	if (_line[0] == '{') {
		if (_line.size() > 1) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": No characters allowed after '{'.");
		}
		_state = SEARCHING_LOCATION_DIRECTIVE;
		if (_debug) {
			std::cout << _lineNum << ": Start '{' of a location." << std::endl;
		}
	} else {
		throw std::invalid_argument("Parsing Error: at line " + lineNum + ": Expected '{'. (Make sure to have the '{' on a new _line.)");
	}
}

// Search for the location directive, parse it and store the data
void ConfigParser::_SearchingLocationDirective() 
{
	// convert the _lineNum into a string for error handling purpose
	std::ostringstream oss;
    oss << _lineNum;
    std::string lineNum = oss.str();

	// Check if it is the end of the location context
	if (_line[0] == '}') {
		if (_line.size() > 1) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": No characters allowed after '}'.");
		}
		_state = SEARCHING_SERVER_DIRECTIVE;
		if (_debug) {
			std::cout << _lineNum << ": End '}' of a location." << std::endl;
		}
	} else {
		// Parse the directive

		// Check there is a ; at the end of the _line
		if (_line[_line.size() - 1] != ';') {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": Expected ';'.");
		}

		// Remove ';' from the end of the _line
		_line.erase(_line.size() - 1);

		// Parse key
		std::string key = _line.substr(0, _line.find_first_of(" \t"));

		// Parse value
		std::string value = _line.substr(key.size());
		_Trim(value);

		if (_debug) {
			std::cout << _lineNum << ": Key: " << key << ", Value: " << value << std::endl;
		}

		try {
			// Check if the key is valid
			if (key == "allow_methods") {
				std::vector<std::string> allowMethods = _GetVectorFromElementsInString(value);
				_servers.back().SetLastLocationAllowMethods(allowMethods);
			} else if (key == "redirection") {
				_servers.back().SetLastLocationRedirection(value);
			} else if (key == "root") {
				_servers.back().SetLastLocationRoot(value);
			} else if (key == "autoindex") {
				if (value == "on") {
					_servers.back().SetLastLocationAutoindex(true);
				} else if (value == "off") {
					_servers.back().SetLastLocationAutoindex(false);
				} else {
					throw std::invalid_argument("Invalid value for 'autoindex'.");
				}
			} else if (key == "default_file") {
				_servers.back().SetLastLocationDefaultFile(value);
			} else if (key == "cgi_ext") {
				std::vector<std::string> cgiExt = _GetVectorFromElementsInString(value);
				_servers.back().SetLastLocationCgiExt(cgiExt);
			} else if (key == "upload_folder") {
				_servers.back().SetLastLocationUploadFolderPath(value);
			} else if (key == "cgi_path") {
				std::vector<std::string> cgiInterpreterPath = _GetVectorFromElementsInString(value);
				_servers.back().SetLastLocationCgiInterpreterPath(cgiInterpreterPath);
			} else {
				throw std::invalid_argument("Invalid key.");
			}
		} catch (const std::exception& e) {
			throw std::invalid_argument("Parsing Error: at line " + lineNum + ": " + std::string(e.what()));
		}
	}
}

// ------------------------- Parsing -------------------------
// Parse the config file and store data to the servers vector
void ConfigParser::_Parse(std::ifstream& file) {
	while (std::getline(file, _line)) {
		_lineNum++;
		_RemoveComments(_line);
		_Trim(_line);
		if (_line.empty()) {
			continue;
		}
		switch (_state) {
			case SEARCHING_SERVER:
				_SearchingServer();
				break;

			case SEARCHING_SEVER_BRACKET:
				_SearchingServerBracket();
				break;

			case SEARCHING_SERVER_DIRECTIVE:
				_SearchingServerDirective();
				break;

			case SEARCHING_LOCATION_BRACKET:
				_SearchingLocationBracket();
				break;

			case SEARCHING_LOCATION_DIRECTIVE:
				_SearchingLocationDirective();
				break;
		}
	}
	if (_debug) {
		std::cout << _lineNum << ": End of file." << std::endl;
	}
}
