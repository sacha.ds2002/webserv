/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   StatusManager.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 10:03:55 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/14 13:29:52 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../webserv.h"
 


// Initializes the map containing all status codes, their reason and description
void    StatusManager::Initialize()
{
    std::ifstream f(STATUS_CSV_PATH);
	std::string line;

    if (!f.is_open())
        throw std::runtime_error("Failed to open the csv status file.");
		
    while (std::getline(f, line))
	{
		std::istringstream iss(line);
        std::string field;
		Status s;
		
		s.SetDescription("");
		if (std::getline(iss, field, ','))
        	s.SetCode(std::atoi(field.c_str()));

		if (std::getline(iss, field, ','))
        	s.SetReason(field);

		if (std::getline(iss, field, ','))
        	s.SetErrorBool(std::atoi(field.c_str()) == 1 ? true : false);

		if (std::getline(iss, field, ','))
        	s.SetDescription(field);
		
		statusMap.insert(std::pair<int, Status>(s.GetCode(), s));
    }

    f.close();
}

// Gets a status struct containing the code, the reason and the body
const Status	&StatusManager::GetStatus(int code)
{
	std::map< int, Status >::iterator it;
	
	it = statusMap.find(code);
	if (it == statusMap.end())
	{
		std::ostringstream oss;
		oss << code;
		std::string codeStr = oss.str();
		throw std::runtime_error("Response status code not found : " + codeStr);
	}
	
	return (*it).second;
}
