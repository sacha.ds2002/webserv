/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/08 11:33:59 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/30 15:33:06 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../webserv.h"

// Static variables
std::map<int, Status > StatusManager::statusMap;

int main(int ac, char **av, char **envp)
{
	try 
	{
		// Initialize
		CGIHandler cgi(envp);
		std::string configFilePath;
		setConfigFilePath(configFilePath, ac, av);
		StatusManager::Initialize();

		// Parse config file
		ConfigParser configParser(configFilePath);
		configParser.printServers();
		
		// Initialize servers
		std::vector<Server> allServers = configParser.GetServers();
		for (size_t i = 0; i < allServers.size(); i++)
			allServers[i].ServerInit(cgi);
	
		// Run servers
		Server::runServers(allServers);

	} 
	catch (const std::exception& e)
	{
		std::cerr << RED << "Error: " << e.what() << RESET << std::endl;
	}

	return 0;
}

