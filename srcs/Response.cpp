/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Response.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/08 11:34:48 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/07 13:11:16 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../webserv.h"


// ------------------------------------ Canonical form ------------------------------------
// Default constructor
Response::Response(void)
{
    _version = "HTTP/1.1";
	_code = "200";
	_reason = "OK";
	_body = "";
}

// Parameters constructor
Response::Response(const std::string &v, const std::string &c, const std::string &r):
    _version(v), _code(c), _reason(r)
{
	if (_version.empty()
		|| _reason.empty() || _version != "HTTP/1.1")
		throw HttpException(500); // Internal server error
    return ;
}

// Copy constructor
Response::Response(const Response &other)
{
    *this = other;
    return ;
}

// Assignment operator overload
Response &Response::operator=(const Response &other)
{
    if (this != &other) {
		_version = other._version;
		_code = other._code;
		_reason = other._reason;
		_body = other._body;
		_header = other._header;
	}
    return (*this);
}

// Destructor
Response::~Response(void)
{
    return ;
}

// ------------------------------------ Setters ------------------------------------

void    Response::SetVersion(const std::string &version)
{
	std::cout << "Version: " << version << std::endl;
	if (version != "HTTP/1.1" || version.empty())
		throw HttpException(500); // Internal server error

    _version = version;
}

void    Response::SetCode(const std::string &code)
{
    _code = code;
}

void    Response::SetReason(const std::string &reason)
{
    _reason = reason;
}

void    Response::SetBody(const std::string &body, const std::string &type)
{
    _body = body;
	int len = body.length();
	std::stringstream ss;
    ss << len;
	AddToHeader("Content-Length", ss.str());
	AddToHeader("Content-Type", type);
}

// ------------------------------------ Getters ------------------------------------
const std::string	&Response::GetVersion(void) const
{
    return this->_version;
}

const std::string	&Response::GetCode(void) const
{
    return this->_code;
}

const std::string	&Response::GetReason(void) const
{
    return this->_reason;
}

const std::string	&Response::GetBody(void) const
{
    return this->_body;
}

// Adds a key-value pair to the header
void    Response::AddToHeader(std::string key, std::string value)
{
    this->_header.insert(std::pair<std::string, std::string>(key, value));
}

// Removes a key-value pair from the Header
void    Response::RemoveFromHeader(const std::string &key)
{
    if (!this->_header.empty())
    {
        if (this->_header.erase(key) > 0)
            std::cout << "Successfully erased elements bound to '"
                << key << "' from response's header." << std::endl; 
    }
}

// Returns a header's value based on key
std::string	Response::GetHeaderValue(const std::string &key) const
{
    if (!this->_header.empty())
    {
        StringMap::const_iterator it;
        
        it = _header.find(key);
        if (it == _header.end())
        {
            std::cout << "No element were found for key '"
                << key << "' in response's header." << std::endl;
            return "";
        }
        return (*it).second;
    }
    return "";
}

// Checks that all needed attributes are set
void	Response::CheckResponse(void) const
{
	if (GetVersion() == "" || GetReason() == "") {
		std::cout << RED << "Version or Reason is empty" << RESET << std::endl;
		throw HttpException(500); // Server internal error
	}

	if (GetBody() != "")
	{
		if (GetHeaderValue("Content-Type") == "" ||
			GetHeaderValue("Content-Length") == "")
		{
			std::cout << RED << "Missing infos in Response header" << RESET << std::endl;
			throw HttpException(500); // Server internal error
		}
	}
}

// Parses the Response object into a string
std::string	Response::ParseResponseToStr(void) const
{
	std::string ret = "";
	
	CheckResponse();

	ret.append(GetVersion());
	ret.append(" ");
	ret.append(GetCode());
	ret.append(" ");
	ret.append(GetReason());
	ret.append("\n");

	ret.append(_ParseHeaderToStr());
	
	if (GetBody() != "")
	{
		ret.append("\n\n");
		ret.append(GetBody());
	}
	
    return ret;
}

std::string	Response::_ParseHeaderToStr(void) const
{
	StringMap::const_iterator it;
	std::string ret = "";
	
	if (_header.size() > 0)
	{
		it = _header.begin();
		while (it != _header.end())
		{
			ret.append((*it).first);
			ret.append(": ");
			ret.append((*it).second);
			it++;
			if (it != _header.end())
				ret.append("\n");
		}
	}
	return ret;
}

// ------------------------------------ Create Response ------------------------------------
void Response::CreateResponse(Request &req, Server &server) 
{
	_version = req.GetVersion();
	_code = req.GetStatusString();
	_reason = StatusManager::statusMap[std::atoi(_code.c_str())].GetReason();

	// Check if the request is a redirection
	// if (req.GetIsRedirection() == true)
	// {
	// 	_CreateRedirection(req);
	// 	return ;
	// }

	// Check if the requested file is a CGI
	if (req.GetIsCGI() == true)
	{
		std::string path = server.GetPwd() + server.GetRoot();

		if (!req.GetUploadFolderPath().empty())
			server.cgiHandler.AddVarToEnv("UPLOAD_DIRECTORY", req.GetUploadFolderPath());
		else
			server.cgiHandler.AddVarToEnv("UPLOAD_DIRECTORY", "/uploads");

		if (req.GetMethod() == "GET")
			SetBody(server.cgiHandler.HandleGetRequest(req, path), "text/html");
		else if (req.GetMethod() == "POST") {
			if (req.GetHeaderValue("Content-Length") == "0")
				throw HttpException(400);
			_code = "201";
			_reason = StatusManager::statusMap[std::atoi(_code.c_str())].GetReason();
			SetBody(server.cgiHandler.HandlePostRequest(req, path), "text/html");
		}
	}		
	else if (req.GetMethod() == "GET") {
		_CreateGET(req);
	} else if (req.GetMethod() == "POST") {
		_CreatePOST();
	} else if (req.GetMethod() == "DELETE") {
		_CreateDELETE(req);
	}
}

void	Response::CreateErrResponse(Status status, Server &server)
{
	_version = "HTTP/1.1";
	_reason = status.GetReason();

	std::ostringstream oss;
	oss << status.GetCode();
	_code = oss.str();

	// Send the corresponding error page from the server error pages map
	std::map<unsigned int, std::string> errorPagesPath = server.GetErrorPagePath();
	// check if the error page exists
	if (errorPagesPath.find(status.GetCode()) != errorPagesPath.end())
	{
		std::string path = server.GetPwd() + errorPagesPath[status.GetCode()];
		std::ifstream file(path.c_str());
		if (file.is_open()) 
		{
			std::string line;
			while (getline(file, line))
				_body += line;
			file.close();
			SetBody(_body, "text/html");
		}
	}
	if (_body.empty()) 
	{
		// If the error page does not exist, create a default error page
		_body = "<html>"
				"<head>"
					"<title>" + _code + " " + _reason + "</title>"
				"</head>"
				"<body>"
					"<h1>" + _code + " " + _reason + "</h1>"
					"<p>" + status.GetDescription() + "</p>"
					"<p>This page was generated automatically.</p>"
				"</body>"
				"</html>";
		SetBody(_body, "text/html");
	}
}


std::string Response::MakeDirList(const std::string& dirPath, const std::string& path) const
{
	std::stringstream ss;
	ss << "<html><head>";
	ss << "<title>Directory Listing</title>";
	ss << "<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">";
	ss << "</head><body>";
	ss << "</header><h1>Directory Listing: " << path << "</h1></header>";
	ss << "<main>";

	DIR* dir = opendir(dirPath.c_str());
	if (dir == NULL)
	{
		ss << "<p>Error: Unable to open directory</p></body></html>";
	}
	else 
	{
		ss << "<section class=\"file-list\">";
		struct dirent* entry;
		while ((entry = readdir(dir)) != NULL) 
		{
			std::string name = entry->d_name;
			if (name != "." && name != ".." && name.find(".") != std::string::npos && name.find("styles.css") == std::string::npos) {
				name = path + "/" + name;
				ss << "<li><a href=\"" << name << "\">" << name << "</a></li>";
			}
		}
		ss << "</section>";
		closedir(dir);
	}
	ss << "</main>";
	ss << "</body></html>";
	return ss.str();
}

// Create a GET response
void Response::_CreateGET(Request &req)
{
	std::string contentType = "text/html";
	// Check if the request asks for a specific content type
	contentType = req.GetContentType(req.GetFullPath());

	// Add cache control to the header
	AddToHeader("Cache-Control", "public, max-age=3600");

	// Check if the server should serve a directory listing
	if (req.GetServeDirectoryListing() == true)
	{
		std::string dirList = MakeDirList(req.GetFullPath(), req.GetPath());
		SetBody(dirList, "text/html");
	}
	else // if not, serve the requested file
	{
		if (contentType == "text/html" || contentType == "text/css" || contentType == "text/javascript" || contentType == "text/plain") {
			std::string path = req.GetFullPath();
			std::ifstream file(path.c_str());
			if (file.is_open()) {
				std::string line;
				while (getline(file, line)) {
					_body += line;
				}
				file.close();
				SetBody(_body, contentType);
			}
		} // if the request asks for an image
		else if (contentType == "image/jpeg" || contentType == "image/png" || contentType == "image/x-icon" || contentType == "image/svg+xml") {
			std::ifstream file(req.GetFullPath().c_str(), std::ios::binary);
			if (file.is_open()) {
				std::ostringstream buffer;
				buffer << file.rdbuf();
				_body = buffer.str();
				file.close();
				SetBody(_body, contentType);
			}
		}
		else {
			throw HttpException(501);
		}
	}
}

// Create a POST response
void Response::_CreatePOST()
{
	SetBody("<html><body><h1>POST request received</h1></body></html>", "text/html");
}

// Delete the requested resource
void Response::_CreateDELETE(Request &req)
{
	std::string path = req.GetFullPath();
	
	if (remove(path.c_str()) != 0)
	{
		// If the file could not be deleted
		throw HttpException(500);
	}
	else
	{
		_code = "204";
		_reason = StatusManager::statusMap[std::atoi(_code.c_str())].GetReason();
		// std::cout << YELLOW << "Deleted file at: " << path << RESET << std::endl;
		SetBody("<html><body><h1>Resource deleted successfully</h1></body></html>", "text/html");
	}
}

// Redirection
void Response::_CreateRedirection(Request &req)
{
	_code = "301";
	_reason = StatusManager::statusMap[std::atoi(_code.c_str())].GetReason();
	AddToHeader("Location", req.GetRedirection());
	SetBody("redirection", "text/html");
}