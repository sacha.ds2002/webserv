/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/14 13:50:35 by etbernar          #+#    #+#             */
/*   Updated: 2024/06/10 15:09:40 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Server.hpp"
#include "../webserv.h"
#include <cstring>

void Server::initAddrData()
{
	memset(&_serverAddr, 0, sizeof(_serverAddr));
	_serverAddr.sin_family = AF_INET;
	_serverAddr.sin_port = htons(GetPort());

	std::string host = GetHost();
	std::cout << "Host: " << host << std::endl;
	if (inet_pton(AF_INET, host.c_str(), &_serverAddr.sin_addr) <= 0) 
	{
    	std::cerr << "Error: Invalid IP address format: " << host << std::endl;
		exit(EXIT_FAILURE);
    }
	_addressLen = sizeof(_serverAddr);
}

// Init all sockets thingys and failsafe for each
void Server::ServerInit(const CGIHandler &cgi)
{
	this->cgiHandler = cgi;

	int reuseAddr = 1;
	initAddrData();

	_serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(_serverSocket < 0)
	{
		std::cerr << "Error: Could not create socket." << std::endl;
		close(_serverSocket);
		exit(EXIT_FAILURE);
	}
	if (setsockopt(_serverSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&reuseAddr, sizeof(reuseAddr)) < 0)
	{
		std::cerr << "Error: Could not set socket options." << std::endl;
		close(_serverSocket);
		exit(EXIT_FAILURE);
	}
	if (fcntl(_serverSocket, F_SETFL, O_NONBLOCK) < 0)
	{
		std::cerr << "Error: Could not set socket to non-blocking with fcntl()" << std::endl;
		close(_serverSocket);
		exit(EXIT_FAILURE);
	}
	if(bind(_serverSocket, (struct sockaddr*)&_serverAddr, _addressLen) < 0)
	{
		std::cerr << RED << "Error: Could not bind socket. " << strerror(errno) << RESET << std::endl;
		close(_serverSocket);
		exit(EXIT_FAILURE);
	}
	if(listen(_serverSocket, 100) < 0)
	{
		std::cerr << "Error: Could not listen on socket." << std::endl;
		close(_serverSocket);
		exit(EXIT_FAILURE);
	}
	std::cout << GREEN << "Server started on port " << _port << RESET << std::endl;
	_clientSocket = -1;
	FD_ZERO(&_allFds);
	FD_ZERO(&_readFds);
	FD_ZERO(&_writeFds);
	_maxSocketNb = _serverSocket;
	FD_SET(_serverSocket, &_allFds);
	_timeOut.tv_sec = 0;
	_timeOut.tv_usec = 0;
}

