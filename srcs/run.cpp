#include "../includes/Server.hpp"
#include "../webserv.h"

void Server::runServers(std::vector<Server> allServers)
{
	while(1) 
	{
		for (size_t i = 0; i < allServers.size(); i++)
			allServers[i].ServerLoop();
	}
}

//Looping server loop
void Server::ServerLoop()
{
	FD_COPY(&_allFds, &_readFds);
	int readyFds = select(_maxSocketNb + 1, &_readFds, &_writeFds, NULL, &_timeOut);
	if (readyFds < 0 || readyFds > FD_SETSIZE)
	{
		std::cerr << "Error: Could not select socket." << std::endl;
		for (int i = 0; i <= _maxSocketNb; i++)
		{
			if (FD_ISSET(i, &_writeFds) && i != _serverSocket)
				FD_CLR(i, &_writeFds);
			if (FD_ISSET(i, &_readFds) && i != _serverSocket)
			{
				FD_CLR(i, &_readFds);
				FD_CLR(i, &_allFds);
				close(i);
			}
		}
		return;
	}

	for (int i = 0; i <= _maxSocketNb; i++)
	{
		if (FD_ISSET(i, &_readFds) && i == _serverSocket)
		{
			_clientSocket = accept(_serverSocket, NULL, NULL);
			if (_clientSocket < 0 && errno != EWOULDBLOCK)
			{
				close(_clientSocket);
				std::cerr << "Error: Fail of accept on fd: " << i << std::endl;
				return;
			}
			else
			{
				FD_SET(_clientSocket, &_allFds);
				if (_maxSocketNb < _clientSocket)
					_maxSocketNb = _clientSocket;
			}
		}
		if (FD_ISSET(i, &_readFds) && i != _serverSocket)
		{
			// std::cout << GREEN << std::endl << "Receiving data" << WHITE << std::endl;
			ReceiveData(i);
		}
		else if (FD_ISSET(i, &_writeFds))
		{
			HandleRequest(i);
			FD_CLR(i, &_writeFds);
		}
	}
}

int Server::UpdateMaxSocketNb()
{
    int maxFd = -1;
    // Iterate through the _allFds set to find the highest file descriptor value
    for (int fd = 0; fd <= _maxSocketNb; fd++)
    {
        if (FD_ISSET(fd, &_allFds))
        {
            maxFd = (maxFd > fd) ? maxFd : fd;
        }
    }
    // Return the highest fd value plus one
    return maxFd + 1;
}

// Receive data from the client and store it in _requestStr
void Server::ReceiveData(int _clientSocket)
{
    int recvData = 0;
    char buff[BUFFER];

    memset(buff, 0, BUFFER);
    recvData = recv(_clientSocket, buff, sizeof(buff), 0);
    if (recvData <= 0) {
        FD_CLR(_clientSocket, &_allFds);
        FD_CLR(_clientSocket, &_readFds);
        FD_CLR(_clientSocket, &_writeFds);
        close(_clientSocket);
        UpdateMaxSocketNb();
        return;
    }
    _requestStr.append(buff, recvData);
   	FD_SET(_clientSocket, &_writeFds);
}

void Server::_UploadFile(Request req)
{
	std::string directoryPath = GetPwd() + GetRoot() + "/uploads";
	std::string fileName;
	std::string fileContent;

	try
	{
		fileName = Utils::ExtractFilename(req.GetBody());
		fileContent = Utils::ExtractFileContent(
			req.GetHeaderValue("Content-Type"), req.GetBody());
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << "Error parsing multipart data: " << e.what() << RESET << std::endl;
		throw HttpException(500);
	}

	std::string filePath = !fileName.empty() ?
		directoryPath + "/" + fileName :
		directoryPath + "/default.txt";

	// Check if the directory exists, if not, create it
	if (!Utils::CreateDirectory(directoryPath))
	{
		std::cerr << "Failed to create directory: " << directoryPath << std::endl;
		throw HttpException(500);
	}

	// Create the file and write content to it
	if (!Utils::CreateFile(filePath, fileContent)) {
		std::cerr << "Failed to create file: " << filePath << std::endl;
		throw HttpException(500);
	}
	// std::cout << YELLOW << "File uploaded to: " << filePath << RESET << std::endl;
}


void Server::HandleRequest(int socketHandle)
{
	try
	{
		// print request string to debug
		// std::cout << MAGENTA << "Request:\n" << _requestStr << RESET << std::endl << std::endl;

		if (_requestStr.empty())
			throw HttpException(400);

		// parse request
		Request req;
		req.ParseStrToReq(_requestStr);
		_requestStr.clear();

		std::cout << std::endl << MAGENTA << "Request: " << req.GetMethod() << " " << req.GetPath() << " " << req.GetVersion() << RESET << std::endl;

		// Check if the request is valid
		req.CheckReq(*this);

		// If there is a file to upload
		// If the Content-Type doesn't contain multipart/form-data and the body
		// doesn't contain "filename", then the request's body isn't a file to upload
		// and might be used as input in the CGI
		if (req.GetMethod() == "POST"
			&& req.GetHeaderValue("Content-Type").find("multipart/form-data") != std::string::npos
			&& req.GetBody().find("filename") != std::string::npos)
			this->_UploadFile(req);

		// create Response object
		Response res;
		res.CreateResponse(req, *this);
		std::string response = res.ParseResponseToStr();

		// print response string to debug
		std::cout << BLUE << "Response: " << res.GetVersion() << " " << res.GetCode() << " " << res.GetReason() << RESET << std::endl << std::endl;

		// send response
		int bytesSent = send(socketHandle, response.c_str(), response.size(), 0);
		if (bytesSent <= 0)
			std::cerr << RED << "Error: Fail of send on fd: " << socketHandle << RESET << std::endl;

		// close socket
		close(socketHandle);
		FD_CLR(socketHandle, &_allFds);
		FD_CLR(socketHandle, &_readFds);
		FD_CLR(socketHandle, &_writeFds);
		_maxSocketNb = UpdateMaxSocketNb();
	}
	catch(const HttpException& e)
	{
		// Build or Get error page using a CGI
		std::cerr << RED << "Caught an HTTP error: " << e.what() << RESET << std::endl;
		Status status = StatusManager::GetStatus(e.code());
		Response errRes;
		errRes.CreateErrResponse(status, *this);
		std::string response = errRes.ParseResponseToStr();

		// send response
		int bytesSent = send(socketHandle, response.c_str(), response.size(), 0);
		if (bytesSent <= 0)
			std::cerr << RED << "Error: Fail of send on fd: " << socketHandle << RESET << std::endl;

		// close socket
		close(socketHandle);
		FD_CLR(socketHandle, &_allFds);
		FD_CLR(socketHandle, &_readFds);
		FD_CLR(socketHandle, &_writeFds);
		_maxSocketNb = UpdateMaxSocketNb();
	}
}
