#include "../includes/Server.hpp"
#include <iostream>
#include <unistd.h>
#include <limits.h>

// ------------------------- Canonical Form -------------------------
// Constructor
Server::Server() {
	// Default values
	_port = 8000;
	_host = "127.0.0.1";
	_root = "/websites/website_1";
	_defaultFile = "index.html";
	_serverName = "default";
	_clientMaxBodySize = 1000000;

	_errorPagePath[401] = "/error_pages/401.html";
	_errorPagePath[403] = "/error_pages/403.html";
	_errorPagePath[404] = "/error_pages/404.html";
	_errorPagePath[405] = "/error_pages/405.html";
	_errorPagePath[413] = "/error_pages/413.html";
	_errorPagePath[500] = "/error_pages/500.html";
	_errorPagePath[501] = "/error_pages/501.html";
	_errorPagePath[505] = "/error_pages/505.html";

	_InitializePwd();
}

// Copy Constructor
Server::Server(const Server& other) {
	*this = other;
}

// Assignment Operator
Server& Server::operator=(const Server& other) {
	if (this != &other) {
		_port = other._port;
		_host = other._host;
		_root = other._root;
		_defaultFile = other._defaultFile;
		_serverName = other._serverName;
		_errorPagePath = other._errorPagePath;
		_clientMaxBodySize = other._clientMaxBodySize;
		_locations = other._locations;

		_serverSocket = other._serverSocket;
		_serverAddr = other._serverAddr;
		_addressLen = other._addressLen;
		_allFds = other._allFds;
		_readFds = other._readFds;
		_writeFds = other._writeFds;
		_clientSocket = other._clientSocket;
		_maxSocketNb = other._maxSocketNb;
		_timeOut = other._timeOut;
		_requestStr = other._requestStr;

		_pwd = other._pwd;
	}
	return *this;
}

// Destructor
Server::~Server() {}

// ------------------------- Static -------------------------
std::set<int> Server::CreateErrorCodes() {
	const int codes[] = {
		400, 401, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417,
		421, 422, 423, 424, 425, 426, 428, 429, 431, 451, 500, 501, 502, 503, 504, 505,
		506, 507, 508, 510, 511
	};
	std::set<int> errorCodeSet(codes, codes + sizeof(codes) / sizeof(codes[0]));
	return errorCodeSet;
}

const std::set<int> Server::_httpErrorCodes = Server::CreateErrorCodes();

// ------------------------- Getters -------------------------
unsigned int Server::GetPort() const { return _port; }
const std::string& Server::GetHost() const { return _host; }
const std::string& Server::GetServerName() const { return _serverName; }
const std::string& Server::GetRoot() const { return _root; }
const std::string& Server::GetDefaultFile() const { return _defaultFile; }
const std::map<unsigned int, std::string>& Server::GetErrorPagePath() const { return _errorPagePath; }
unsigned int Server::GetClientMaxBodySize() const { return _clientMaxBodySize; }
const std::vector<t_location>& Server::GetLocations() const { return _locations; }
const std::string& Server::GetPwd() const { return _pwd; }

// ------------------------- Setters -------------------------
void Server::SetPort(int p) {
	if (p > 0 && p <= 65535) {
		_port = p;
	} else {
		throw std::invalid_argument("Invalid port number.");
	}
}

void Server::SetPort(const std::string& p) {
	// Check if there is no other characters than digits
	for (size_t i = 0; i < p.length(); i++) {
		if (p[i] < '0' || p[i] > '9') {
			throw std::invalid_argument("Invalid port number.");
		}
	}
	if (!p.empty()) {
		int port = 0;
		try {
			port = std::atoi(p.c_str());
		} catch (const std::exception& e) {
			throw std::invalid_argument("Invalid port number: " + std::string(e.what()));
		}
		if (port > 0 && port <= 65535) {
				_port = port;
			} else {
				throw std::invalid_argument("Invalid port number.");
			}
	} else {
		throw std::invalid_argument("Port number cannot be empty.");
	}
}

void Server::SetHost(const std::string& h) {
	if (!h.empty()) {
		if (_IsValidIPAddress(h)) {
			_host = h;
		} else {
			throw std::invalid_argument("Invalid host IP address format.");
		}
	} else {
		throw std::invalid_argument("Host name cannot be empty.");
	}
}

void Server::SetServerName(const std::string& name) {
	if (!name.empty()) {
		_serverName = name;
	} else {
		throw std::invalid_argument("Server name cannot be empty.");
	}
}

void Server::SetRoot(const std::string& root) {
	if (!root.empty()) {
		_root = root;
	} else {
		throw std::invalid_argument("Root path cannot be empty.");
	}
}

void Server::SetDefaultFile(const std::string& file) {
	if (!file.empty()) {
		_defaultFile = file;
	} else {
		throw std::invalid_argument("Default file name cannot be empty.");
	}
}

void Server::SetErrorPagePath(const std::map<unsigned int, std::string>& errors) {
	if (!errors.empty()) {
		_errorPagePath = errors;
	} else {
		throw std::invalid_argument("Error page paths cannot be empty.");
	}
}

void Server::SetClientMaxBodySize(int size) {
	if (size > 0) {
		_clientMaxBodySize = size;
	} else {
		throw std::invalid_argument("Client max body size must be positive.");
	}
}

void Server::SetClientMaxBodySize(const std::string& size) {
	// Check if there is no other characters than digits
	for (size_t i = 0; i < size.length(); i++) {
		if (size[i] < '0' || size[i] > '9') {
			throw std::invalid_argument("Invalid port number.");
		}
	}
	if (!size.empty()) {
		int s = 0;
		try {
			s = std::atoi(size.c_str());
		} catch (const std::exception& e) {
			throw std::invalid_argument("Client max body size must be a number: " + std::string(e.what()));
		}
		if (s > 0) {
			_clientMaxBodySize = s;
		} else {
			throw std::invalid_argument("Client max body size must be positive.");
		}
	} else {
		throw std::invalid_argument("Client max body size cannot be empty.");
	}
}

void Server::SetLocations(const std::vector<t_location>& locs) {
	if (!locs.empty()) {
		_locations = locs;
	} else {
		throw std::invalid_argument("Locations list cannot be empty.");
	}
}

void Server::SetLastLocationRoute(const std::string& route) {
	if (!route.empty()) {
		_locations.back().route = route;
	} else {
		throw std::invalid_argument("Route cannot be empty.");
	}
}

void Server::SetLastLocationAllowMethods(const std::vector<std::string>& methods) {
	if (!methods.empty()) {	
		// Empty the list
		_locations.back().allowMethods.clear();
		// Check that the methods are valid
		for (size_t i = 0; i < methods.size(); i++) {
			if (methods[i] != "GET" && methods[i] != "POST" && methods[i] != "DELETE") {
				throw std::invalid_argument("Invalid method: '" + methods[i] + "'. Allowed methods are GET POST DELETE.");
			}
		}
		_locations.back().allowMethods = methods;
	} else {
		throw std::invalid_argument("Methods list cannot be empty.");
	}
}

void Server::SetLastLocationRedirection(const std::string& redirection) {
	if (!redirection.empty()) {
		_locations.back().redirection = redirection;
	} else {
		throw std::invalid_argument("Redirection cannot be empty.");
	}
}

void Server::SetLastLocationRoot(const std::string& root) {
	if (!root.empty()) {
		_locations.back().root = root;
	} else {
		throw std::invalid_argument("Root cannot be empty.");
	}
}

void Server::SetLastLocationAutoindex(bool autoindex) {
	_locations.back().autoindex = autoindex;
}

void Server::SetLastLocationDefaultFile(const std::string& defaultFile) {
	if (!defaultFile.empty()) {
		_locations.back().defaultFile = defaultFile;
	} else {
		throw std::invalid_argument("Index cannot be empty.");
	}
}

void Server::SetLastLocationCgiExt(const std::vector<std::string>& cgiExt) {
	if (!cgiExt.empty()) {
		_locations.back().cgiExt.clear();
		_locations.back().cgiExt = cgiExt;
	} else {
		throw std::invalid_argument("CGI extensions list cannot be empty.");
	}
}

void Server::SetLastLocationUploadFolderPath(const std::string& uploadFolderPath) {
	if (!uploadFolderPath.empty()) {
		_locations.back().uploadFolderPath = uploadFolderPath;
	} else {
		throw std::invalid_argument("Upload folder path cannot be empty.");
	}
}

void Server::SetLastLocationCgiInterpreterPath(const std::vector<std::string>& cgiInterpreterPath) {
	if (!cgiInterpreterPath.empty()) {
		_locations.back().cgiInterpreterPath = cgiInterpreterPath;
	} else {
		throw std::invalid_argument("CGI interpreter path list cannot be empty.");
	}
}

// ------------------------- Setters-like -------------------------
void Server::UpdateErrorPagePath(int code, const std::string& path) {
    if (path.empty()) {
        throw std::invalid_argument("Error path cannot be empty.");
    }
    if (_httpErrorCodes.find(code) == _httpErrorCodes.end()) {
        throw std::invalid_argument("Invalid error code.");
    }

    _errorPagePath[code] = path;
}

void Server::UpdateErrorPagePath(const std::string& code, const std::string& path) {
	// check that the code is a number
	for (size_t i = 0; i < code.length(); i++) {
		if (code[i] < '0' || code[i] > '9') {
			throw std::invalid_argument("Error code must be a number.");
		}
	}

	if (code.empty()) {
		throw std::invalid_argument("Error code cannot be empty.");
	}
	if (path.empty()) {
		throw std::invalid_argument("Error path cannot be empty.");
	}
	int c = 0;
	try {
		c = std::atoi(code.c_str());
	} catch (const std::exception& e) {
		throw std::invalid_argument("Error code must be a number: " + std::string(e.what()));
	}

	if (_httpErrorCodes.find(c) == _httpErrorCodes.end()) {
		throw std::invalid_argument("Invalid error code.");
	}
	_errorPagePath[c] = path;
}

void Server::AddLocation() {
	t_location loc;

	loc.route = "/";
	loc.allowMethods.push_back("GET");
	loc.redirection = "";
	loc.root = _root;
	loc.autoindex = false;
	loc.defaultFile = _defaultFile;
	loc.cgiExt.push_back(".py");
	loc.uploadFolderPath = "/uploads";
	loc.cgiInterpreterPath.push_back("/usr/bin/python3");

	_locations.push_back(loc);
}

// ------------------------- Checkers -------------------------
// Check if the IP address is valid
bool Server::_IsValidIPAddress(const std::string& ip) {
	if (ip == "localhost") {
		return true;
	}
	if (ip.length() < 7 || ip.length() > 15) {
		return false;
	}
	for (size_t i = 0; i < ip.length(); i++) {
		if (ip[i] != '.' && (ip[i] < '0' || ip[i] > '9')) {
			return false;
		}
	}
	std::vector<std::string> parts;
	std::string part;
	for (size_t i = 0; i < ip.length(); i++) {
		if (ip[i] == '.') {
			parts.push_back(part);
			part.clear();
		} else {
			part += ip[i];
		}
	}
	parts.push_back(part);
	if (parts.size() != 4) {
		return false;
	}
	for (size_t i = 0; i < parts.size(); i++) {
		if (parts[i].length() < 1 || parts[i].length() > 3) {
			return false;
		}
		int num = std::atoi(parts[i].c_str());
		if (num < 0 || num > 255) {
			return false;
		}
	}

	return true;
}

// ------------------------- Helpers -------------------------
// Print the server information
void Server::PrintInfo() const {
	std::cout << "Server Information: " << std::endl;
	std::cout << "Port: " << _port << std::endl;
	std::cout << "Host: " << _host << std::endl;
	std::cout << "Server Name: " << _serverName << std::endl;
	std::cout << "Root: " << _root << std::endl;
	std::cout << "Default File: " << _defaultFile << std::endl;
	std::cout << "Error Page Path: " << std::endl;
	for (std::map<unsigned int, std::string>::const_iterator it = _errorPagePath.begin(); it != _errorPagePath.end(); it++) {
		std::cout << "\t" << it->first << ": " << it->second << std::endl;
	}
	std::cout << "Client Max Body Size: " << _clientMaxBodySize << std::endl;
	std::cout << "Locations: " << std::endl;
	for (size_t i = 0; i < _locations.size(); i++) {
		std::cout << "\t" << "Location " << i + 1 << ": " << std::endl;
		std::cout << "\t" << "Route: " << _locations[i].route << std::endl;
		std::cout << "\t" << "Allow Methods: " << std::endl;
		for (size_t j = 0; j < _locations[i].allowMethods.size(); j++) {
			std::cout << "\t\t" << _locations[i].allowMethods[j] << std::endl;
		}
		std::cout << "\t" << "Redirection: " << _locations[i].redirection << std::endl;
		std::cout << "\t" << "Root: " << _locations[i].root << std::endl;
		std::cout << "\t" << "Autoindex: " << (_locations[i].autoindex ? "true" : "false") << std::endl;
		std::cout << "\t" << "Default File: " << _locations[i].defaultFile << std::endl;
		std::cout << "\t" << "CGI Extensions: " << std::endl;
		for (size_t j = 0; j < _locations[i].cgiExt.size(); j++) {
			std::cout << "\t\t" << _locations[i].cgiExt[j] << std::endl;
		}
		std::cout << "\t" << "Upload Folder Path: " << _locations[i].uploadFolderPath << std::endl;
		if (i != _locations.size() - 1) {
			std:: cout << std::endl;
		}
	}
	std::cout << std::endl;
}

// Initialize pwd
void Server::_InitializePwd() {
	char cwd[1024];
	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		_pwd = cwd;
	} else {
		throw std::runtime_error("Error getting current working directory.");
	}
}