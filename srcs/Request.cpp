/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Request.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/08 11:34:43 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/14 15:01:07 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../webserv.h"
#include "../includes/Server.hpp"
#include <sys/stat.h>

// ------------------------------------- CANONICAL FORM -------------------------------------
// Default constructor
Request::Request(void)
{
	_method = "";
	_path = "";
	_version = "";
	_body = "";
	_fullPath = "";
	_queryString = "";
	_statusCode = 200;
	_location = t_location();
	_serveDirectoryListing = false;
	_isRedirection = false;
	_isCGI = false;

	mime_types[".txt"] = "text/plain";
	mime_types[".html"] = "text/html";
	mime_types[".css"] = "text/css";
	mime_types[".js"] = "text/javascript";
	mime_types[".jpg"] = "image/jpeg";
	mime_types[".jpeg"] = "image/jpeg";
	mime_types[".png"] = "image/png";
	mime_types[".ico"] = "image/x-icon";
	mime_types[".svg"] = "image/svg+xml";
    return ;
}

// Parameters constructor
Request::Request(const std::string &m, const std::string &p, const std::string &v):
    _method(m), _path(p), _version(v)
{
    return ;
}

// Copy constructor
Request::Request(const Request &other)
{
    *this = other;
    return ;
}

// Assignment operator overload
Request &Request::operator=(const Request &other)
{
    if (this != &other) {
		_method = other._method;
		_path = other._path;
		_version = other._version;
		_body = other._body;
		_header = other._header;
		_fullPath = other._fullPath;
		_queryString = other._queryString;
		_statusCode = other._statusCode;
		_location = other._location;
		_serveDirectoryListing = other._serveDirectoryListing;
		_isRedirection = other._isRedirection;
	}
    return (*this);
}

// Destructor
Request::~Request(void)
{
    return ;
}

// ------------------------------------- SETTERS -------------------------------------	

void    Request::SetMethod(const std::string &method)
{
    _method = method;
}

void    Request::SetPath(const std::string &path)
{
    _path = path;
}

void    Request::SetVersion(const std::string &version)
{
    _version = version;
}

void    Request::SetBody(std::string body)
{
    _body = body;
}

// ------------------------------------- GETTERS -------------------------------------

const std::string	&Request::GetMethod(void) const
{
    return this->_method;
}

const std::string	&Request::GetPath(void) const
{
    return this->_path;
}

const std::string	&Request::GetVersion(void) const
{
    return this->_version;
}

const std::string	&Request::GetBody(void) const
{
    return this->_body;
}

const std::string	&Request::GetFullPath(void) const
{
	return this->_fullPath;
}

const std::string	&Request::GetQueryString(void) const
{
	return this->_queryString;
}

int	Request::GetStatusCode(void) const
{
	return this->_statusCode;
}

std::string	Request::GetStatusString(void) const
{
	std::ostringstream oss;          // Create an output string stream
    oss << this->_statusCode;               // Insert the number into the stream
	return oss.str();
}

bool				Request::GetServeDirectoryListing(void) const
{
	return this->_serveDirectoryListing;
}

bool				Request::GetIsRedirection(void) const
{
	return this->_isRedirection;
}

const std::string	&Request::GetRedirection(void) const
{
	return this->_location.redirection;
}

const std::string	&Request::GetUploadFolderPath(void) const
{
	return this->_location.uploadFolderPath;
}

bool 				Request::GetIsCGI(void) const
{
	return this->_isCGI;
}

void    Request::AddToHeader(const std::string &key, const std::string &value)
{
    this->_header.insert(std::pair<std::string, std::string>(key, value));
    
    // std::cout << "Added key '" << key << "' with value '"
    //     << value << "' to request's header." << std::endl;
}

void    Request::RemoveFromHeader(const std::string &key)
{
    if (!this->_header.empty())
    {
        if (this->_header.erase(key) > 0)
            std::cout << "Successfully erased elements bound to '"
                << key << "' from request's header." << std::endl; 
    }
}

std::string	Request::GetHeaderValue(const std::string &key) const
{
    if (!this->_header.empty())
    {
        StringMap::const_iterator it;
        
        it = _header.find(key);
        if (it == _header.end())
        {
            std::cout << "No element were found for key '"
                << key << "' in request's header." << std::endl;
            return "";
        }
        return (*it).second;
    }
    return "";
}

// ------------------------------------- CHECK REQUEST -------------------------------------
// Check if the request as a whole is valid
void    Request::CheckReq(Server server)
{
	_statusCode = _IsPathValid(server); // Check if the path is valid and set _fullPath and _location if it is one
	if (_statusCode >= 300)
		throw HttpException(_statusCode);
	_statusCode = _IsMethodValid();
	if (_statusCode >= 300)
		throw HttpException(_statusCode);
	_statusCode = _IsVersionValid();
	if (_statusCode >= 300)
		throw HttpException(_statusCode);
	_statusCode = _IsContentLengthValid(server);
	if (_statusCode >= 300)
		throw HttpException(_statusCode);
	_statusCode = _IsContentTypeValid();
	if (_statusCode >= 300)
		throw HttpException(_statusCode);
	_statusCode = _ExtraChecks();
	if (_statusCode >= 300)
		throw HttpException(_statusCode);

	// Check if it is a CGI
	_IsCGI();
}

// Check if the request's path is valid
// If the path is valid, set _fullPath, and _location if the path is a location
// If the path is a directory and the default file does not exist and autoindex is set to true, set _serveDirectoryListing to true
int	Request::_IsPathValid(Server server)
{
	// Check if the asked resource is the root
	if (_path == "/") {
		_fullPath = server.GetPwd() + server.GetRoot() + "/" + server.GetDefaultFile();
		return 200;
	}
	
	//Trim query from string and insert it in _queryString
	size_t pos = _path.find("?");
    if (pos != std::string::npos)
	{
		_queryString = _path.substr(pos + 1, _path.length() - pos);
        _path = _path.substr(0, pos);
	}

	// Check if the asked resource is a location
	std::vector<t_location> locations = server.GetLocations();
	std::vector<t_location>::iterator it = locations.begin();
	while (it != locations.end())
	{
		// If the path starts with the location's route
		if (_path.substr(0, (*it).route.size()) == (*it).route && (_path[(*it).route.size()] == '/' || _path[(*it).route.size()] == '\0'))
		{
			_fullPath = server.GetPwd() + (*it).root + _path;

			// Check if the path is valid
			if (access(_fullPath.c_str(), 0) == -1) {
				// If the path is not valid, check if the location has a redirection
				if ((*it).redirection != "") 
				{
					_fullPath = server.GetPwd() + (*it).root + (*it).redirection;
					if (access(_fullPath.c_str(), 0) == -1)
						return 404;
				}
				else
					return 404;
			}
			// Check if the path is a directory
			struct stat sb;
			if (stat(_fullPath.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
				// create a temp string to path to the directory
				std::string tempPath = _fullPath;
				if (_path[_path.size() - 1] != '/')
					_fullPath += "/";
				_fullPath += (*it).defaultFile;
				
				// Check if the default file exists
				if (access(_fullPath.c_str(), 0) == -1) {
					// Check if the location autoindex is set to true
					if ((*it).autoindex == true) {
						_serveDirectoryListing = true;
						_fullPath = tempPath;
						return 200;
					}
					return 403;
				}
			}
			_location = *it;

			if (_location.redirection != "")
				_isRedirection = true;

			return 200;
		}
		it++;
	}

	// If the path is not a location, check if the ressource might still exist in the server root
	_fullPath = server.GetPwd() + server.GetRoot() + _path;
	if (access(_fullPath.c_str(), 0) == -1)
		return 404;

	// Check if the path is a directory
	struct stat sb;
	if (stat(_fullPath.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
		if (_path[_path.size() - 1] != '/')
			_fullPath += "/";
		_fullPath += server.GetDefaultFile();
		
		// Check if the default file exists
		if (access(_fullPath.c_str(), 0) == -1)
			return 403;
	}

	return 200;
}

// Check if the request's Content-Length header is valid
int	Request::_IsContentLengthValid(Server server)
{
	if (GetMethod() == "POST")
	{
		// Check if the Content-Length header is present
		StringMap::const_iterator it;
		it = _header.find("Content-Length");
		if (it == _header.end()) {
			// Length Required Error
			return 411;
		}
		
		// Check if the Content-Length header is valid
		try {
			unsigned int contentLength = std::atoi(((*it).second).c_str());
			if (contentLength > server.GetClientMaxBodySize()) {
				// Payload Too Large Error
				return 413;
			}
		} catch (const std::exception& e) {
			std::cout << RED << "Content-Length too big" << std::endl;
			return 400;
		}
	}
	return 200;
}

// Check if the request's method is valid
int	Request::_IsMethodValid()
{
	if (_method != "GET" && _method != "POST" && _method != "DELETE")
		return 405;

	// Check if the method is allowed in the location
	if (_location.route != "") // Make sure the path is a location therefore that the location is set
	{
		std::vector<std::string>::iterator it = _location.allowMethods.begin();
		while (it != _location.allowMethods.end())
		{
			if (*it == _method)
				return 200;
			it++;
		}
		return 405;
	}
	return 200;
}

// Check if the request's version is valid
int	Request::_IsVersionValid()
{
	if (_version != "HTTP/1.1")
		return 505;
	return 200;
}

// Check if the request's content type is valid
int	Request::_IsContentTypeValid()
{
	if (GetMethod() == "POST")
	{
		// Check if the Content-Type header is present
		StringMap::const_iterator it;
		it = _header.find("Content-Type");
		if (it == _header.end()) {
			// Unsupported Media Type Error
			return 415;
		}
		
		// Check if the Content-Type header is valid　(i'll add more types later)
		std::string contentType = (*it).second;
		// accept txt files
		// if (contentType != "text/html" && contentType != "application/x-www-form-urlencoded" && contentType != "text/plain")
		// 	return 415;
	}
	return 200;
}

// Extra checks
int Request::_ExtraChecks()
{
	// Check if that the path is not a directory when the method is DELETE
	if (GetMethod() == "DELETE")
	{
		struct stat sb;
		if (stat(_fullPath.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
			return 403;
	}
	return 200;
}

// is the request a CGI request
void Request::_IsCGI()
{
	// go through the location.cgiExt vector and check if the path ends with any of the extensions
	std::vector<std::string>::iterator it = _location.cgiExt.begin();
	while (it != _location.cgiExt.end())
	{
		if (_fullPath.size() >= (*it).size() && _fullPath.compare(_fullPath.size() - (*it).size(), (*it).size(), *it) == 0)
		{
			_isCGI = true;
			return ;
		}
		it++;
	}

	// if the request is not a location, check if the path ends with any of the extensions
	if (_location.route == "") {
		if (_fullPath.find(".py") != std::string::npos || _fullPath.find(".php") != std::string::npos)
			_isCGI = true;
	}
}

// ------------------------------------- PARSING REQUEST -------------------------------------
// Parses a string to set this Request instance's attributes
void    Request::ParseStrToReq(const std::string &str)
{
	int state = 0; // 0: first line, 1: header part, 2: body part
	
	std::istringstream iss(str);
    std::string line;
	std::string body = "";
	
	while (std::getline(iss, line, '\n') && state != -1)
	{
		std::istringstream ss(line);
		std::string field;
		std::string key;
		std::string value;
		switch (state)
		{			
			case 0:
				if (std::getline(ss, field, ' '))
					SetMethod(field);
				if (std::getline(ss, field, ' '))
					SetPath(field);
				if (std::getline(ss, field, ' '))
				{
                    field.erase(field.find_last_not_of(" \n\r\t")+1); // remove trailing whitespaces
                    SetVersion(field);
                }
				if (GetMethod() == "" || GetPath() == "" || GetVersion() == "")
					throw HttpException(400); // Bad request (No body but method is POST)
				state = 1; // Go to header part by setting state to 1
				break;
			case 1:
				if (!Utils::StringIsEmptyLine(line))
				{
					if (std::getline(ss, field, ' '))
						key = field.erase(field.size() - 1);
					if (std::getline(ss, field, '\n'))
					{
						field.erase(field.find_last_not_of(" \n\r\t")+1); // remove trailing whitespaces
						value = field;
					}
					AddToHeader(key, value);
				}
				else
					state = 2;
				break;
			case 2:
				// maybe we have to change the way we get the body because of the \r\n
				body.append(line);
				body.append("\n");
				break;
			default:
				break;
		}
	}
	SetBody(body);
    return ;
}

std::string	Request::_ParseHeaderToStr(void) const
{
	StringMap::const_iterator it;
	std::string ret = "";
	
	if (_header.size() > 0)
	{
		it = _header.begin();
		while (it != _header.end())
		{
			ret.append((*it).first);
			ret.append(": ");
			ret.append((*it).second);
			it++;
			if (it != _header.end())
				ret.append("\n");
		}
	}
	return ret;
}

std::string	Request::ParseReqToStr(void)
{
	std::string ret = "";

	ret.append(GetMethod());
	ret.append(" ");
	ret.append(GetPath());
	ret.append(" ");
	ret.append(GetVersion());
	ret.append("\n");
	
	ret.append(_ParseHeaderToStr());

	if (GetMethod() != "GET" && GetBody() != "")
	{
		ret.append("\n\n");
		ret.append(GetBody());
	}
	
    return ret;
}

// ------------------------------------- HELPER -------------------------------------
// Get the extension of a file
std::string Request::GetExtension(const std::string &path) const
{
	size_t pos = path.find_last_of(".");
	if (pos == std::string::npos)
		return "";
	return path.substr(pos);
}

// Get the content type of a file
std::string Request::GetContentType(const std::string &path) const
{
	std::string extension = GetExtension(path);
	if (mime_types.find(extension) != mime_types.end())
		return mime_types.find(extension)->second;
	return "application/octet-stream";
}