/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Status.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etbernar <etbernar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/14 10:16:20 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/28 09:48:58 by etbernar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../webserv.h"

Status::Status()
{ }

Status::Status(int c, const std::string &r, const std::string &d, bool error):
	_code(c),_isError(error), _reason(r), _description(d) 
{ }

// Copy constructor
Status::Status(const Status &other):
	_code(other.GetCode()), _isError(other.GetIsError()), _reason(other.GetReason()), _description(other.GetDescription())
{
    return ;
}

// Assignment operator overload
Status &Status::operator=(const Status &other)
{
    _code = other.GetCode();
	_reason = other.GetReason();
	_description = other.GetDescription();
	_isError = other.GetIsError();
    return (*this);
}

// Destructor
Status::~Status(void)
{
    return ;
}

int	Status::GetCode(void) const
{
	return (_code);
}

bool	Status::GetIsError(void) const
{
	return (_isError);
}

const std::string	&Status::GetReason(void) const
{
	return (_reason);
}

const std::string	&Status::GetDescription(void) const
{
	return (_description);
}

void    Status::SetCode(int code)
{
    _code = code;
}

void    Status::SetErrorBool(bool error)
{
    _isError = error;
}

void    Status::SetReason(const std::string &reason)
{
    _reason = reason;
}

void    Status::SetDescription(std::string desc)
{
    _description = desc;
}
