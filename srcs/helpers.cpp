/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/06/06 16:35:29 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/06 16:35:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../webserv.h"

void setConfigFilePath(std::string& configFilePath, int ac, char **av) {
	if (ac == 1) {
		configFilePath = "./config/default.conf";
	} else if (ac == 2) {
		configFilePath = av[1];
	} else {
		throw std::runtime_error("Too many arguments");
	}
}