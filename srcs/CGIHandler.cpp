/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CGIHandler.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 10:39:22 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/14 16:11:34 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include  "../webserv.h"


// Default constructor
CGIHandler::CGIHandler(void)
{
    return ;
}

// Parameters constructor, parse the envp received from main into a StringMap
CGIHandler::CGIHandler(char **envp)
{
	_ParseEnvpToMap(envp);
    return ;
}

// Copy constructor
CGIHandler::CGIHandler(const CGIHandler &other)
{
    *this = other;
    return ;
}

// Assignment operator overload
CGIHandler &CGIHandler::operator=(const CGIHandler &other)
{
    this->_envMap = other._envMap;
    return (*this);
}

// Destructor
CGIHandler::~CGIHandler(void)
{
    return ;
}

void CGIHandler::AddVarToEnv(std::string key, std::string value)
{
	if (!key.empty())
		this->_envMap.insert(std::pair<std::string, std::string>(key, value));
}

void CGIHandler::RemoveFromEnv(std::string key)
{
	if (!this->_envMap.empty())
    {
        if (this->_envMap.erase(key) <= 0)
			std::cout << "Element " << key << " not found in env map." << std::endl;
    }
}

std::string	CGIHandler::GetVarValue(const std::string &key) const
{
	 if (!this->_envMap.empty())
    {
        StringMap::const_iterator it;
        
        it = _envMap.find(key);
        if (it == _envMap.end())
        {
            std::cout << "No element were found for key '"
                << key << "' in env variables." << std::endl;
            return "";
        }
        return (*it).second;
    }
    return "";
}

char **CGIHandler::EnvToCharArray(void) const
{
	StringMap::const_iterator it;
	char **ret;
	
	if (_envMap.size() > 0)
	{
		ret = (char **)malloc((_envMap.size() + 1) * sizeof(char *));
		ret[_envMap.size()] = NULL;
		
		int i = 0;
		for (it = _envMap.begin(); it != _envMap.end(); it++)
		{
			std::string tmp = "";
			tmp.append((*it).first);
			tmp.append("=");
			tmp.append((*it).second);
			ret[i] = (char *)malloc((tmp.size() + 1) * sizeof(char));
            strcpy(ret[i], tmp.c_str());
			i++;
		}
		return ret;
	}
	return NULL;
}

void CGIHandler::_ParseEnvpToMap(char **envp)
{
	int i = -1;
	while (envp[++i] != NULL)
	{
		std::istringstream iss(envp[i]);
		std::string field;
		std::string key;
		std::string value;
		if (std::getline(iss, field, '='))
			key = field.erase(field.size());
		if (std::getline(iss, field, '\n'))
			value = field;
		AddVarToEnv(key, value);
	}
}

// Returns the body of a CGI for a GET request
std::string	CGIHandler::HandleGetRequest(Request req, const std::string &path)
{
	int fds[2];
	pid_t pid;
	std::string ret = "";

	if (pipe(fds) == -1)
		throw HttpException(500);
	
	pid = fork();
	if (pid == -1)
		throw HttpException(500);
	else if (pid == 0)
	{
		// Manage fds
		dup2(fds[1], STDOUT_FILENO);
		close(fds[0]);
		close(fds[1]);
		
		// Add query string to env if there is one
		if (req.GetQueryString() != "")
			this->AddVarToEnv("QUERY_STRING", req.GetQueryString());

		this->AddVarToEnv("REQUEST_METHOD", "GET");

		if (chdir(path.c_str()) == -1)
			exit(-1);

		char* const args[] = { const_cast<char*>(req.GetFullPath().c_str()), NULL };

		// Execute CGI using request's path and Env map to char** parsing function
		if (execve(req.GetFullPath().c_str(), args, this->EnvToCharArray()) == -1)
			exit(-1);
	}
	else
	{
		// Parent process
        // Close the write end of the pipe
        close(fds[1]);

        // Set up time tracking
        time_t start_time = time(NULL);
        time_t current_time;

        // Read from the pipe (stdout of the child process) with non-blocking I/O
        std::string output;
        char buffer[4096];
        ssize_t bytes_read;
        fcntl(fds[0], F_SETFL, O_NONBLOCK); // Set the read end of the pipe to non-blocking mode

        while (true)
		{
            // Check if timeout has been reached
            current_time = time(NULL);
            if (current_time - start_time >= 5)
			{
                kill(pid, SIGKILL); // Kill child process
                waitpid(pid, NULL, 0); // Reap the killed child process
                close(fds[0]); // Close the read end of the pipe
				std::cerr << RED << "CGI Timeout reached." << RESET << std::endl;
                throw HttpException(408);
            }
			
            // Read data from the pipe if available
            bytes_read = read(fds[0], buffer, sizeof(buffer));
            if (bytes_read > 0)
                output.append(buffer, bytes_read);
			else if (bytes_read == 0) // End of file (child process has exited)
                break;
        }

        // Close the read end of the pipe
        close(fds[0]);

        // Wait for the child process to terminate
        int status;
        waitpid(pid, &status, 0);
		if (WIFEXITED(status))
		{
		    int exitStatus = WEXITSTATUS(status);
			if (exitStatus != 0)
			{
				// The process exited with a non-zero status code, indicating an error
				std::cerr << RED << "CGI process exited with status code: "
					<< exitStatus << RESET << std::endl;
				throw HttpException(500);
			}
		}
		else 
			throw HttpException(500);

        // Return the output from the CGI script
        return output;
    }
	return "";
}

// Returns the body of a CGI for a POST request
std::string	CGIHandler::HandlePostRequest(Request req, const std::string &path)
{
	int outpipe[2]; // Pipe for CGI output
    int inpipe[2];  // Pipe for CGI input
	pid_t pid;
	int status;

	if (pipe(inpipe) == -1 || pipe(outpipe) == -1)
		throw HttpException(500);
	
	pid = fork();
	if (pid == -1)
		throw HttpException(500);
	else if (pid == 0)
	{
        // Manage fds
        dup2(outpipe[1], STDOUT_FILENO);
        close(outpipe[0]);

        dup2(inpipe[0], STDIN_FILENO); // Redirect stdin to inpipe[0] first
        if (req.GetHeaderValue("Content-Type").find("multipart/form-data") == std::string::npos)
			write(inpipe[1], req.GetBody().c_str(), req.GetBody().length()); // Write request body
        close(inpipe[1]); // Close inpipe[1] after writing
		
		// Add query string to env if there is one
		if (req.GetQueryString() != "")
			this->AddVarToEnv("QUERY_STRING", req.GetQueryString());
		
		this->AddVarToEnv("REQUEST_METHOD", "POST");

		if (chdir(path.c_str()) == -1)
			exit(-1);

		char* const args[] = { const_cast<char*>(req.GetFullPath().c_str()), NULL };
		
		// Execute CGI using request's path and Env map to char** parsing function
		if (execve(req.GetFullPath().c_str(), args, this->EnvToCharArray()) == -1)
			exit(-1);
	}
	else
	{
		// Manage fds
		close(outpipe[1]);
		close(inpipe[0]);
        close(inpipe[1]);

		 // Set up time tracking
        time_t start_time = time(NULL);
        time_t current_time;

        // Read from the pipe (stdout of the child process) with non-blocking I/O
        std::string output;
        char buffer[4096];
        ssize_t bytes_read;
        fcntl(outpipe[0], F_SETFL, O_NONBLOCK); // Set the read end of the pipe to non-blocking mode

		while (true)
		{
            // Check if timeout has been reached
            current_time = time(NULL);
            if (current_time - start_time >= 5)
			{
                kill(pid, SIGKILL); // Kill child process
                waitpid(pid, NULL, 0); // Reap the killed child process
                close(outpipe[0]); // Close the read end of the pipe
				std::cerr << RED << "CGI Timeout reached." << RESET << std::endl;
                throw HttpException(408);
            }
			
            // Read data from the pipe if available
            bytes_read = read(outpipe[0], buffer, sizeof(buffer));
            if (bytes_read > 0)
                output.append(buffer, bytes_read);
			else if (bytes_read == 0) // End of file (child process has exited)
                break;
        }

        // Close the read end of the pipe
        close(outpipe[0]);

        // Wait for the child process to terminate
        waitpid(pid, &status, 0);
		if (WIFEXITED(status))
		{
		    int exitStatus = WEXITSTATUS(status);
			if (exitStatus != 0)
			{
				// The process exited with a non-zero status code, indicating an error
				std::cerr << RED << "CGI process exited with status code: "
					<< exitStatus << RESET << std::endl;
				throw HttpException(500);
			}
		}
		else 
			throw HttpException(500);
        // Return the output from the CGI script
        return output;
	}
	return "";
}