/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   webserv.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/08 11:34:54 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/08 11:34:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ********************************************** */
// PROJECT MADE BY SADA-SIL, EHOLZER and ETBERNAR //
/* ********************************************** */

#ifndef WEBSERV_H
# define WEBSERV_H

// # define RESET			""
// # define RED			""
// # define GREEN			""
// # define YELLOW			""
// # define BLUE			""
// # define MAGENTA		""
// # define CYAN			""
// # define WHITE			""

# define RESET			"\033[0m"
# define RED			"\033[0;31m"
# define GREEN			"\033[0;32m"
# define YELLOW			"\033[0;33m"
# define BLUE			"\033[0;34m"
# define MAGENTA		"\033[0;35m"
# define CYAN			"\033[0;36m"
# define WHITE			"\033[0;37m"

# define BUFFER 5000000

# define STATUS_CSV_PATH "srcs/status.csv" 

// Includes
# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <cstring>
# include <iomanip>
# include <vector>
# include <list>
# include <set>
# include <map>
# include <cmath>
# include <algorithm>
# include <utility>
# include <iterator>
# include <unistd.h>
# include <errno.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>
# include <sys/select.h>
# include <fcntl.h>
# include <sys/wait.h>
# include <signal.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <dirent.h>

# ifndef FD_COPY
#  define FD_COPY(src, dest) memcpy((dest), (src), sizeof(*(dest)))
# endif

// Typedefs
// A map with double string value is named StringMap
typedef std::map<std::string, std::string> StringMap;

// Custom includes
# include "includes/HttpException.hpp"
# include "includes/Status.hpp"
# include "includes/StatusManager.hpp"
# include "includes/Utils.hpp"
# include "includes/Request.hpp"
# include "includes/Response.hpp"
# include "includes/CGIHandler.hpp"
# include "includes/Server.hpp"
# include "includes/ConfigParser.hpp"

// Utils
void setConfigFilePath(std::string& configFilePath, int ac, char **av);

#endif