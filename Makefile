# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/05/08 11:36:26 by sada-sil          #+#    #+#              #
#    Updated: 2024/05/08 11:36:26 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# ********************************************** #
# PROJECT MADE BY SADA-SIL, EHOLZER and ETBERNAR #
# ********************************************** #


# COLORS
RESET_COLOR = "\033[0m"
RED = "\033[0;31m"
GREEN = "\033[0;32m"
YELLOW = "\033[0;33m"
BLUE = "\033[0;34m"
MAGENTA = "\033[0;35m"
CYAN = "\033[0;36m"
WHITE = "\033[0;37m"
CLRALL = "\033[2J\033[3J\033[H"      # Clear terminal (supprime historique)
CLR = "\033c"                        # Clear terminal (garde historique)
CLRL = "\033[2K"                     # Clear ligne terminal
BOLD = "\033[1m"

NAME = webserv

CC = c++
CFLAGS = -Wall -Wextra -Werror -std=c++98
RM = rm -rf

SRC_DIR = srcs
OBJ_DIR = objs
INC_DIR = includes

SRCS = $(SRC_DIR)/Server.cpp \
	   $(SRC_DIR)/main.cpp \
       $(SRC_DIR)/helpers.cpp \
       $(SRC_DIR)/ConfigParser.cpp \
       $(SRC_DIR)/init.cpp \
	   $(SRC_DIR)/run.cpp \
	   $(SRC_DIR)/Request.cpp \
	   $(SRC_DIR)/Response.cpp \
	   $(SRC_DIR)/Status.cpp \
	   $(SRC_DIR)/StatusManager.cpp \
	   $(SRC_DIR)/CGIHandler.cpp \
	   $(SRC_DIR)/Utils.cpp

OBJS = $(SRCS:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@mkdir -p $(OBJ_DIR)
	@echo ${CYAN}"Compiling "${MAGENTA}"[Webserv]"${CYAN}" $< ..."${WHITE}
	@$(CC) $(CFLAGS) -c $< -o $@

$(NAME): $(OBJS)
	@$(CC) $(CFLAGS) $(OBJS) -o $@
	@echo ${GREEN}"Compilation [Webserv] successful!"${WHITE}

all: $(NAME)

clean:
	@echo ${RED}"Starting "${MAGENTA}"[Webserv]"${RED}" Cleaning objects..."${WHITE}
	@$(RM) $(OBJS)
	@$(RM) $(OBJ_DIR)
	@echo ${RED}"Clean "${MAGENTA}"[Webserv]"${RED}" complete."${WHITE}

fclean: clean
	@echo ${RED}"Removing "${MAGENTA}"[Webserv]"${RED}" executable..."${WHITE}
	@$(RM) $(NAME)
	@echo ${RED}"Executable "${MAGENTA}"[Webserv]"${RED}" removed."${WHITE}

re: fclean all
