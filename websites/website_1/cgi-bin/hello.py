#!/usr/bin/env python3
import os
import cgi

query_string = os.environ.get('QUERY_STRING', '')

# Parse the query string into a dictionary
form = cgi.FieldStorage(environ={'QUERY_STRING': query_string})
# Access individual query parameters
name = form.getvalue('name', '')

print("<html>")
print("<head>")
print("<title>Hello, World!</title>")
print("</head>")
print("<body>")
print("<h1>Hello, World from CGI !</h1>")
if name != '':
	print("<h2>%s, we welcome you to our Webserv !<h2>" % (name,))
print("</body>")
print("</html>")