#!/usr/bin/env python3

import os

# Get the current working directory for uploads
upload_folder = os.getenv("UPLOAD_DIRECTORY")
uploads_dir = ""
if not upload_folder:
    uploads_dir = os.getcwd() + "/uploads"
else:
    uploads_dir = os.getcwd() + upload_folder

# Function to list uploaded files
def list_files():
    output = """
        <html>
        <head>
            <script>
                function validateForm() {
                    var fileInput = document.getElementById('fileInput');
                    if (fileInput.files.length === 0) {
                        alert('Please select a file before uploading.');
                        return false; // Prevent form submission
                    }
                    return true; // Allow form submission
                }
                
                function deleteFile(filePath) {
                    console.log('Attempting to delete file:', filePath);
                    fetch(filePath, {
                        method: 'DELETE'
                    })
                    .then(response => {
                        console.log('Received response with status:', response.status);
                        if (response.status === 204) {
                            window.location.replace(window.location.pathname);
                        } else if (response.status === 404) {
                            alert('File not found.');
                        } else {
                            alert('An error occurred: ' + response.statusText);
                        }
                    })
                    .catch(error => console.error('Error:', error));
                }
            </script>
            <title>File Upload CGI</title>
            <link rel="stylesheet" type="text/css" href="/styles.css">
        </head>
        <body>
            <h1>File Upload</h1>
            <p><em>better than Google Drive.</em></p>
            &nbsp;
            <p>Check out different routes below:</p>
            <a href="/">Home</a>
            <a href="route1">Route 1</a>
            <a href="route2">Route 2</a>
            <a href="route3">Route 3</a>
            &nbsp;
            <h3>Upload a file:</h3>
            <form method="post" enctype="multipart/form-data" action="" onsubmit="return validateForm()">
                <input type="file" name="file" id="fileInput">
                <input type="submit" value="Upload">
            </form>&nbsp;"""
    if os.path.exists(uploads_dir):
        files = os.listdir(uploads_dir)
        if files:
            output += """<h3>Uploaded files:</h3><p>click on the file to download it</p>&nbsp;"""
            for file_name in files:
                file_path = os.path.join("/uploads", file_name)
                output += """
                <div>
                    <a href="{file_path}" download="{file_name}">{file_name}</a>
                    <button onclick="deleteFile('{file_path}')">Delete</button>
                </div>
                <br>
                """.format(file_name=file_name,file_path=file_path)
            return output
        else:
            output += "No files have been uploaded yet."
            return output
    else:
        output += "The uploads directory does not exist."
        return output

print(list_files())

print("""
</body>
</html>
""")
