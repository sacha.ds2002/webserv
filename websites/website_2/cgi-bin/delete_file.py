#!/usr/bin/env python3

import os
import cgi
import cgitb
import json
import sys

cgitb.enable()

uploads_dir = "uploads"  # Make sure this path is correct for your setup

def delete_file(file_name):
    try:
        if file_name:
            file_path = os.path.join(uploads_dir, file_name)
            if os.path.exists(file_path):
                os.remove(file_path)
                return "File {} deleted successfully.".format(file_name)
            else:
                return "File {} does not exist.".format(file_name)
        else:
            return "No file specified."
    except Exception as e:
        error_message = "An error occurred: {}\n{}".format(str(e), traceback.format_exc())
        print("<p>{}</p>".format(error_message))
        return "An error occurred while deleting the file."

# Read JSON data from request body
try:
    content_length = int(os.environ['CONTENT_LENGTH'])
    request_body = sys.stdin.read(content_length)
    data = json.loads(request_body)
    file_name = data.get("file_name", "")

    print("Content-Type: text/html")
    print()
    print("<html><body>")
    if os.environ['REQUEST_METHOD'] == 'DELETE':
        print(delete_file(file_name))
    else:
        print("Unsupported method.")
    print('<br><a href="/cgi-bin/list_files.py">Go back to list</a>')
    print("</body></html>")
except Exception as e:
    error_message = "An error occurred: {}\n{}".format(str(e), traceback.format_exc())
    print("Content-Type: text/html")
    print()
    print("<html><body>")
    print("<p>{}</p>".format(error_message))
    print("</body></html>")
