#ifndef LOCATION_HPP
#define LOCATION_HPP

#include <string>
#include <vector>

typedef struct location {
	std::string 				route;
	std::vector<std::string>	allowMethods;
	std::string					redirection;
	std::string					root;
	bool						autoindex;
	std::string					defaultFile;
	std::vector<std::string>	cgiExt;
	std::string					uploadFolderPath;
	std::vector<std::string>	cgiInterpreterPath;

	location()
        : route(""), redirection(""), root(""), autoindex(false),
          defaultFile(""), uploadFolderPath("") {}
} t_location;

#endif