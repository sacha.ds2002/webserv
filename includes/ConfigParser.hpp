#ifndef CONFIGPARSING_HPP
#define CONFIGPARSING_HPP
#include "Server.hpp"
#include <vector>
#include <fstream>

// Parser State
typedef enum state {
	SEARCHING_SERVER,
	SEARCHING_SEVER_BRACKET,
	SEARCHING_SERVER_DIRECTIVE,
	SEARCHING_LOCATION_BRACKET,
	SEARCHING_LOCATION_DIRECTIVE
} e_state;

class Server;

class ConfigParser {
public:
	// Canonical Form
	ConfigParser(const std::string& configFilePath);
	ConfigParser(const ConfigParser& other);
	ConfigParser &operator=(const ConfigParser &other);
	~ConfigParser();

	// Getters
	std::vector<Server> GetServers();

	// Setters
	void SetServers(const std::vector<Server>& servers);

	// Helpers
	void printServers() const;

private:
	std::vector<Server>	_servers;
	e_state				_state;
	std::string			_serverKeyword;
	std::string			_locationKeyword;
	int					_lineNum;
	std::string			_line;
	bool				_debug;
	
	void _Parse(std::ifstream& file);

	// Helpers
	void _RemoveComments(std::string& line);
	void _Trim(std::string& str);
	std::vector<std::string> _GetVectorFromElementsInString(std::string& value);

	// States
	void _SearchingServer();
	void _SearchingServerBracket();
	void _SearchingServerDirective();
	void _SearchingLocationBracket();
	void _SearchingLocationDirective();
};

#endif