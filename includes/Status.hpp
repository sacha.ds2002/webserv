/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Status.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/14 10:16:36 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/14 12:00:28 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STATUS_HPP
# define STATUS_HPP

# include "../webserv.h"

class Status
{
	public:
		Status();
		Status(int c, const std::string &r, const std::string &d, bool error);
        Status(const Status& other);
        Status &operator=(const Status &other);
        ~Status();

		int 				GetCode(void) const;
		bool				GetIsError(void) const;
		const std::string	&GetReason(void) const;
		const std::string	&GetDescription(void) const;

		void	SetCode(int code);
		void	SetErrorBool(bool error);
		void	SetReason(const std::string &reason);
		void	SetDescription(std::string desc);

	private:
		int			_code;
		bool      	_isError;
		std::string _reason;
		std::string _description;
};

#endif