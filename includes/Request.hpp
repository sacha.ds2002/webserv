/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Request.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/08 11:35:27 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/07 13:09:16 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REQUEST_HPP
# define REQUEST_HPP
# include "../webserv.h"
# include "Server.hpp"
# include "location.hpp"

class Server;

class Request
{
	public:
		Request();
		Request(const std::string &m, const std::string &p, const std::string &v);
        Request(const Request& other);
        Request &operator=(const Request &other);
        ~Request();
		
		// GETTERS - SETTERS
		void				SetMethod(const std::string &method);
		void				SetPath(const std::string &path);
		void				SetVersion(const std::string &version);
		void				SetBody(std::string body);
		const std::string	&GetMethod(void) const;
		const std::string	&GetPath(void) const;
		const std::string	&GetVersion(void) const;
		const std::string	&GetBody(void) const;
		const std::string	&GetFullPath(void) const;
		const std::string	&GetQueryString(void) const;
		int					GetStatusCode(void) const;
		std::string	GetStatusString(void) const;
		bool				GetServeDirectoryListing(void) const;
		bool				GetIsRedirection(void) const;
		const std::string	&GetRedirection(void) const;
		const std::string	&GetUploadFolderPath(void) const;
		bool 				GetIsCGI(void) const;
		
		// HEADER UTILS
		void				AddToHeader(const std::string &key, const std::string &value);
		void				RemoveFromHeader(const std::string &key);
		std::string			GetHeaderValue(const std::string &key) const;

		// CHECK FUNCTIONS
		void				CheckReq(Server server);

		// PARSING FUNCTIONS
		void				ParseStrToReq(const std::string &str);
		std::string			ParseReqToStr(void);

		std::map<std::string, std::string>	mime_types;

		// HELPER FUNCTIONS
		std::string GetExtension(const std::string &path) const;
		std::string GetContentType(const std::string &path) const;
		
	private:
		std::string	_method;
		std::string _path;
		std::string _version;
		std::string _body;
		
		StringMap	_header;

		std::string _fullPath;
		std::string	_queryString;
		int			_statusCode;
		t_location	_location;
		bool		_serveDirectoryListing;
		bool		_isRedirection;
		bool		_isCGI;

		std::string	_ParseHeaderToStr(void) const;

		// CHECK FUNCTIONS
		int	_IsMethodValid();
		int	_IsPathValid(Server server);
		int	_IsVersionValid();
		int	_IsContentLengthValid(Server server);
		int	_IsContentTypeValid();
		int _ExtraChecks();
		void		_IsCGI();
};

#endif