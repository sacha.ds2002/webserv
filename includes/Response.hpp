/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Response.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/08 11:36:00 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/06 16:39:07 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RESPONSE_HPP
# define RESPONSE_HPP
# include "../webserv.h"

class Response
{
	public:
		Response();
		Response(const std::string &v, const std::string &c, const std::string &r);
        Response(const Response& other);
        Response &operator=(const Response &other);
        ~Response();
		
		// GETTERS - SETTERS
		void				SetVersion(const std::string &version);
		void				SetCode(const std::string &code);
		void				SetReason(const std::string &reason);
		void				SetBody(const std::string &body, const std::string &type);
		const std::string	&GetVersion(void) const;
		const std::string	&GetCode(void) const;
		const std::string	&GetReason(void) const;
		const std::string	&GetBody(void) const;
		
		// HEADER UTILS
		void				AddToHeader(std::string key, std::string value);
		void				RemoveFromHeader(const std::string &key);
		std::string			GetHeaderValue(const std::string &key) const;

		// PARSING FUNCTIONS
		void				CheckResponse(void) const;
		std::string			ParseResponseToStr(void) const;

		// CREATE RESPONSE
		void				CreateResponse(Request &req, Server &server);
		void				CreateErrResponse(Status status, Server &server);
		
		std::string MakeDirList(const std::string& dirPath, const std::string& path) const;


	private:
		std::string	_version;
		std::string	_code;
		std::string _reason;
		std::string _body;
		
		StringMap	_header;

		std::string	_ParseHeaderToStr(void) const;

		// CREATE RESPONSE
		void _CreateGET(Request &req);
		void _CreatePOST();
		void _CreateDELETE(Request &req);
		void _CreateRedirection(Request &req);
};

#endif