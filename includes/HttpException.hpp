/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HttpException.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/06/06 16:18:29 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/06 16:39:47 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef HTTP_EXCEPTION_HPP
# define HTTP_EXCEPTION_HPP
# include "../webserv.h"
# include <sstream>
# include <exception>
# include <string>

class HttpException : public std::exception
{
    public:
        HttpException(int code) : error_code(code)
        {
            std::ostringstream oss;          // Create an output string stream
            oss << error_code;               // Insert the number into the stream
            error_message = oss.str();       // Get the string representation
        }

        virtual ~HttpException() throw() {}

        virtual const char* what() const throw()
        {
            return error_message.c_str();
        }

        int code() const
        {
            return error_code;
        }

    private:
        int error_code;
        std::string error_message;            // Store the error message as a member variable
};

#endif
