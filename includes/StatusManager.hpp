/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   StatusManager.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 09:04:47 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/14 13:13:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STATUSMANAGER_HPP
# define STATUSMANAGER_HPP
# include "../webserv.h"

class StatusManager
{
	public:
		static void			Initialize();
		static const Status	&GetStatus(int code);
		static std::map< int, Status > statusMap;
};

#endif