/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CGIHandler.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 10:39:22 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/06 16:20:40 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CGIHANDLER_HPP
# define CGIHANDLER_HPP

class Request;

# include  "../webserv.h"

class CGIHandler
{
	public:
		// Canonical Form
		CGIHandler();
		CGIHandler(char **envp);
		CGIHandler(const CGIHandler& other);
		CGIHandler& operator=(const CGIHandler& other);
		~CGIHandler();

		void AddVarToEnv(std::string key, std::string value);
		void RemoveFromEnv(std::string key);
		std::string	GetVarValue(const std::string &key) const;
		char **EnvToCharArray(void) const;
		
		// Request handlers
		std::string	HandleGetRequest(Request req, const std::string &path);
		std::string	HandlePostRequest(Request req, const std::string &path);

	private:
		StringMap _envMap;

		void _ParseEnvpToMap(char **envp);
};

#endif
