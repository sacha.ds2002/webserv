/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Utils.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 15:19:14 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/10 15:08:27 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_HPP
# define UTILS_HPP
# include "../webserv.h"

namespace Utils
{
	std::string ReadFileWithFd(int fd);
	bool CreateDirectory(const std::string& path);
	bool CreateFile(const std::string& path, const std::string& content);
	std::string ExtractFilename(const std::string& body);
	std::string ExtractFileContent(const std::string &contentType, const std::string &body);
	bool StringIsEmptyLine(const std::string& str);
}

#endif