/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Server.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 15:25:26 by sada-sil          #+#    #+#             */
/*   Updated: 2024/06/10 15:08:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_HPP
# define SERVER_HPP

# include "CGIHandler.hpp"

# include "../webserv.h"
# include "location.hpp"

class Server {
public:
	// Canonical Form
	Server();
	Server(const Server& other);
	Server& operator=(const Server& other);
	~Server();

	// Getters
	unsigned int								GetPort() const;
	const std::string&							GetHost() const;
	const std::string&							GetServerName() const;
	const std::string&							GetRoot() const;
	const std::string&							GetDefaultFile() const;
	const std::map<unsigned int, std::string>&	GetErrorPagePath() const;
	unsigned int								GetClientMaxBodySize() const;
	const std::vector<t_location>&				GetLocations() const;
	const std::string&							GetPwd() const;

	// Setters
	void SetPort(int p);
	void SetPort(const std::string& p);
	void SetHost(const std::string& h);
	void SetServerName(const std::string& name);
	void SetRoot(const std::string& root);
	void SetDefaultFile(const std::string& file);
	void SetErrorPagePath(const std::map<unsigned int, std::string>& errors);
	void SetClientMaxBodySize(int size);
	void SetClientMaxBodySize(const std::string& size);
	void SetLocations(const std::vector<t_location>& locs);

	void SetLastLocationRoute(const std::string& route);
	void SetLastLocationAllowMethods(const std::vector<std::string>& allowMethods);
	void SetLastLocationRedirection(const std::string& redirection);
	void SetLastLocationRoot(const std::string& root);
	void SetLastLocationAutoindex(bool autoindex);
	void SetLastLocationDefaultFile(const std::string& defaultFile);
	void SetLastLocationCgiExt(const std::vector<std::string>& cgiExt);
	void SetLastLocationUploadFolderPath(const std::string& uploadFolderPath);
	void SetLastLocationCgiInterpreterPath(const std::vector<std::string>& cgiInterpreterPath);

	// Setters-like
	void UpdateErrorPagePath(int code, const std::string& path);
	void UpdateErrorPagePath(const std::string& code, const std::string& path);
	void AddLocation();

	// Helpers
	void PrintInfo() const;
	static std::set<int> CreateErrorCodes();

	//Sockets Part
	void ServerInit(const CGIHandler &cgi);
	void ServerLoop();
	int UpdateMaxSocketNb();

	void ReceiveData(int socketRecieve);
	void HandleRequest(int socketHandle);

	void initAddrData();
	static void runServers(std::vector<Server> allServers);

	// std::string MakeDirList(const std::string& dirPath);
	CGIHandler							cgiHandler;

private:
	// Config variables
    unsigned int						_port;
    std::string							_host;
    std::string							_serverName;
	std::string							_root;
	std::string							_defaultFile;
    std::map<unsigned int, std::string>	_errorPagePath;
    unsigned int						_clientMaxBodySize;
    std::vector<t_location>				_locations;

	// Other variables
	std::string							_pwd;

	// Static variables
	static const std::set<int>			_httpErrorCodes;

	int 								_serverSocket;
	struct sockaddr_in					_serverAddr;
	int 								_addressLen;
	fd_set								_allFds;
	fd_set								_readFds;
	fd_set								_writeFds;
	int									_clientSocket;
	int									_maxSocketNb;
	struct timeval 						_timeOut;
	std::string							_requestStr;

	// Checkers
	bool _IsValidIPAddress(const std::string& ip);

	// Helpers
	void _InitializePwd();
	void _UploadFile(Request req);
};

#endif